pipeline
{
    /*
     * Run everything on master node.
     */
    agent any // It represents the whole pipeline will run on any available agent.
    environment {
        //Use Pipeline Utility Steps plugin to read information from pom.xml into env variables
        def pom = readMavenPom file: 'pom.xml'
        def DevelopmentVersion = pom.version.toString()
        def ReleasedVersion = pom.version.replace('-SNAPSHOT', '')
        def artifactid = pom.getArtifactId()
        PATH=sh(script:"echo $PATH:/usr/local/bin", returnStdout:true).trim()
        JAVA_HOME='/usr/lib/jvm/java-11-openjdk-11.0.10.0.9-1.el7_9.x86_64'
    }
    stages {
        // Prints the environment variables for Mambu Customer Onbarding
        stage('Print Environment variables') {
            steps {
                script {
                    sh 'echo ==========================================='
                    sh 'echo DevelopmentVersion is: $DevelopmentVersion'
                    sh 'echo ReleasedVersion is: $ReleasedVersion'
                    sh 'echo artifactid is: $artifactid'
                    sh 'echo ==========================================='
                }
            }
        }

        stage('Update Version') {
            steps {
                sh 'git checkout main'
                sh 'git pull --force'
                script {
                    env.NewVersion = sh (script: 'bash ./version.sh $ReleasedVersion $(git log -1 --pretty=%B | head -n 3 | tail -n 1)', returnStdout: true).trim()
                }
                sh 'echo New Version is: $NewVersion'
                sh 'mvn versions:set versions:commit -DnewVersion="$NewVersion"'
            }
        }

        stage('Build') {
            steps {
                // Run the maven build
                sh 'mvn clean package'
            }
        }

        stage('Release') {
            when {
                expression {
                    return env.ReleasedVersion != env.NewVersion;
                }
            }
            steps {
                // Release
                script {
                    env.GitUrl = sh (script: 'git config --get remote.origin.url | awk -F"https:\\/\\/" "{print \\$2}"', returnStdout: true).trim()
                }
                withCredentials([usernamePassword(credentialsId: '825f73c1-9cea-4944-9da2-7eae53f2d5c4', usernameVariable: 'GIT_USER', passwordVariable: 'GIT_PASS')]) {
                    script {
                        env.encodedPass=URLEncoder.encode(GIT_PASS, "UTF-8")
                    }
                    sh 'git config user.name "Raunak1998"'
                    sh 'git config user.email "raunakkankaria@hotmail.com"'
                    sh 'git add .'
                    sh "git commit -m 'Update version to $NewVersion'"
                    sh 'git push https://${GIT_USER}:${encodedPass}@${GitUrl} main'
                }
                script {
                    withCredentials([string(credentialsId: 'aa15c75b-5cc8-40fa-9059-caa755ecba3c', variable: 'GIT_TOKEN')]) {
                        env.fileUrl = sh (script: 'curl -s --request POST --header "PRIVATE-TOKEN: $GIT_TOKEN" --form "file=@version.sh" "https://gitlab.com/api/v4/projects/Raunak1998%2Fonlinepizza/uploads" | jq -r ".full_path"', returnStdout: true).trim()
                        sh """curl -s --request POST --header 'PRIVATE-TOKEN: $GIT_TOKEN' --data 'ref=main' https://gitlab.com/api/v4/projects/Raunak1998%2Fonlinepizza/repository/tags?tag_name=${env.NewVersion}"""
                        sh """curl -s --request POST --header 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $GIT_TOKEN' \
                        --data '{ "name": "${env.NewVersion}", "tag_name": "${env.NewVersion}", "assets": { "links": [{ "name": "${env.NewVersion}", "url": "https://gitlab.com${env.fileUrl}", "link_type":"package" }] } }' https://gitlab.com/api/v4/projects/Raunak1998%2Fonlinepizza/releases"""
                    }
                }
                sh 'echo fileURL: $fileUrl'
            }
        }
	}
}
